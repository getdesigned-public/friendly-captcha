<?php
$EM_CONF['friendly_captcha'] = [
    'title' => 'Friendly Captcha',
    'description' => 'Introduces a Friendly Captcha form element',
    'category' => 'fe',
    'version' => '12.4.3-dev',
    'state' => 'stable',
    'author' => 'Daniel Haring',
    'author_company' => 'Getdesigned GmbH',
    'author_email' => 'dh@getdesigned.at',
    'constraints' => [
        'depends' => [
            'core' => '12.4.0-12.99.99',
            'extbase' => '12.4.0-12.99.99',
            'fluid' => '12.4.0-12.99.99',
            'form' => '12.4.0-12.99.99'
        ]
    ]
];