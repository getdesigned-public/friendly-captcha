/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

/**
 * Module: TYPO3/CMS/FriendlyCaptcha/Backend/FormEditor/FriendlyCaptchaViewModel
 *
 * @since 11.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
define(function () {
    'use strict';

    return (function () {

        /**
         * The form render engine invoking this module.
         *
         * @type {{}}
         * @private
         */
        var _formEditorApp = null;

        /**
         * Initializes the module.
         *
         * @param {{}} formEditorApp The form render engine invoking this module
         */
        function bootstrap(formEditorApp) {
            _formEditorApp = formEditorApp;
            _subscribeEvents();
        }

        /**
         * Subscribes to events required for this module.
         *
         * @private
         */
        function _subscribeEvents() {
            _formEditorApp.getPublisherSubscriber().subscribe(
                'view/stage/abstract/render/template/perform',
                _renderFormElement
            );
        }

        /**
         * Renders a single form element.
         *
         * @param {string} topic
         * @param {[]} params
         * @private
         */
        function _renderFormElement(topic, params) {
            if ('FriendlyCaptcha' === params[0].get('type')) {
                _formEditorApp.getViewModel().getStage().renderSimpleTemplateWithValidators(params[0], params[1]);
            }
        }

        /**
         * Export public methods.
         */
        return {
            bootstrap: bootstrap
        };

    })();
});