<?php

declare(strict_types=1);
namespace Getdesigned\FriendlyCaptcha\ViewHelpers\Form;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Fluid\ViewHelpers\Form\AbstractFormFieldViewHelper;

/**
 * View Helper rendering a Friendly Captcha field.
 *
 * @since 11.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class FriendlyCaptchaViewHelper extends AbstractFormFieldViewHelper
{
    /**
     * The Extension TypoScript settings.
     *
     * @var array
     */
    protected array $settings;

    /**
     * @inheritdoc
     */
    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerUniversalTagAttributes();
        $this->registerArgument('start', 'string', 'The way the check should be initialized (either "auto", "focus" or "none").', false, 'auto');
    }

    /**
     * @inheritdoc
     */
    public function initialize()
    {
        parent::initialize();

            // load TypoScript settings
        $this->settings = $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,
            'FriendlyCaptcha'
        );
    }

    /**
     * @inheritdoc
     */
    public function render(): string
    {
        if (empty($siteKey = (string)($this->settings['siteKey'] ?? ''))) {
            return '<div style="background-color: red;">No site key specified!</div>';
        }

        if ($this->settings['bundle'] ?? false) {
            $this->provideJavaScriptBundle();
        }

        if (!in_array($this->arguments['start'], ['auto', 'focus', 'none'])) {
            $this->arguments['start'] = 'auto';
        }

        $name = $this->getName();
        $this->registerFieldNameForFormTokenGeneration($name);

        $class = GeneralUtility::trimExplode(' ', (string)($this->arguments['class'] ?? ''), true);
        if (!in_array('frc-captcha', $class)) {
            array_unshift($class, 'frc-captcha');
        }

        $this->tag->forceClosingTag(true);
        $this->tag->addAttribute('class', implode(' ', $class));
        $this->tag->addAttribute('data-sitekey', $siteKey);
        $this->tag->addAttribute('data-solution-field-name', $name);
        $this->tag->addAttribute('data-start', $this->arguments['start']);
        $this->tag->addAttribute('data-lang', $this->getSiteLanguage()->getTwoLetterIsoCode() ?: 'en');

        if (!empty($puzzleEndpoint = (string)($this->settings['puzzleEndpoint'] ?? ''))) {
            $this->tag->addAttribute('data-puzzle-endpoint', $puzzleEndpoint);
        }

        return $this->tag->render();
    }

    /**
     * Provides the JavaScript bundle for handling the Friendly Captcha widget.
     *
     * @return void
     */
    protected function provideJavaScriptBundle(): void
    {
        static $provided = false;
        if ($provided) {
            return;
        }

        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->addJsLibrary(
            'friendly-captcha',
            'EXT:friendly_captcha/Resources/Public/JavaScript/Frontend/Captcha/friendly-captcha.bundle.min.js',
            'text/javascript',
            false,
            false,
            '',
            true,
            '|',
            true
        );

        $provided = true;
    }

    /**
     * Returns the current site language.
     *
     * @return SiteLanguage The current site language
     */
    protected function getSiteLanguage(): SiteLanguage
    {
        return $this->getServerRequest()->getAttribute('language');
    }

    /**
     * Returns the server request.
     *
     * @return ServerRequestInterface The server request
     */
    protected function getServerRequest(): ServerRequestInterface
    {
        return $GLOBALS['TYPO3_REQUEST'];
    }
}