<?php

declare(strict_types=1);
namespace Getdesigned\FriendlyCaptcha\FormProtection;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

/**
 * Interface for captcha based form protection.
 *
 * @since 11.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
interface CaptchaFormProtectionInterface
{
    /**
     * Verifies the user is a real human by processing the captcha.
     *
     * @param mixed $value The value passed by the user while solving the captcha
     * @return bool TRUE if the user is confirmed to be a real human, otherwise FALSE
     */
    public function challenge($value): bool;
}