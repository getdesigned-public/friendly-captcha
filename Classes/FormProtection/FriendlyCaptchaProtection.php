<?php

declare(strict_types=1);
namespace Getdesigned\FriendlyCaptcha\FormProtection;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use GuzzleHttp\Exception\ClientException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\Exception as ConfigurationException;

/**
 * Service for protecting forms via Friendly Captcha.
 *
 * @since 11.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class FriendlyCaptchaProtection extends AbstractCaptchaFormProtection
{
    /**
     * @inheritdoc
     */
    public function challenge($value): bool
    {
        if (!GeneralUtility::isValidUrl($endpoint = (string)($this->settings['apiEndpoint'] ?? ''))) {
            throw new ConfigurationException('Invalid API endpoint specified.', 1679053908);
        }
        if (empty($apiKey = (string)($this->settings['apiKey'] ?? ''))) {
            throw new ConfigurationException('No API key specified.', 1679053979);
        }
        if (empty($siteKey = (string)($this->settings['siteKey'] ?? ''))) {
            throw new ConfigurationException('No Site key specified.', 1679054081);
        }

        if (
            !is_string($value)
            || empty($value)
            || in_array($value, ['.ERROR', '.FETCHING', '.UNFINISHED', '.UNSTARTED'])
        ) {
            return false;
        }

        try {
            $response = $this->requestFactory->request($endpoint, 'POST', [
                'headers' => [
                    'Accept' => 'application/json'
                ],
                'json' => [
                    'solution' => $value,
                    'secret' => $apiKey,
                    'siteKey' => $siteKey
                ]
            ]);
        } catch (ClientException $exception) {
            return false;
        }

        if (
            200 !== $response->getStatusCode()
            || !is_array($responseData = json_decode($response->getBody()->getContents(), true))
        ) {
            return false;
        }

        return (bool)($responseData['success'] ?? false);
    }
}