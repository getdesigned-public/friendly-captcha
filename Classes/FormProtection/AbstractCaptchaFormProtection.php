<?php

declare(strict_types=1);
namespace Getdesigned\FriendlyCaptcha\FormProtection;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use TYPO3\CMS\Core\Http\RequestFactory;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

/**
 * Base class for captcha based form protection.
 *
 * @since 11.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
abstract class AbstractCaptchaFormProtection implements CaptchaFormProtectionInterface
{
    /**
     * The Extbase Configuration Manager.
     *
     * @var ConfigurationManagerInterface
     */
    protected ConfigurationManagerInterface $configurationManager;

    /**
     * Factory for creating server requests.
     *
     * @var RequestFactory
     */
    protected RequestFactory $requestFactory;

    /**
     * The TypoScript settings to apply.
     *
     * @var array
     */
    protected array $settings;

    /**
     * Dependency injection.
     *
     * @param ConfigurationManagerInterface $configurationManager The Extbase Configuration Manager being injected
     * @param RequestFactory $requestFactory The factory for creating server requests
     */
    public function __construct(ConfigurationManagerInterface $configurationManager, RequestFactory $requestFactory)
    {
        $this->configurationManager = $configurationManager;
        $this->requestFactory = $requestFactory;

            // load settings
        $this->settings = $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,
            'FriendlyCaptcha'
        );
    }
}