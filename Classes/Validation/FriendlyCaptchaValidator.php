<?php

declare(strict_types=1);
namespace Getdesigned\FriendlyCaptcha\Validation;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Getdesigned\FriendlyCaptcha\FormProtection\CaptchaFormProtectionInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator;

/**
 * Validates a Friendly Captcha field.
 *
 * @since 11.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class FriendlyCaptchaValidator extends AbstractValidator
{
    /**
     * @inheritdoc
     */
    protected $acceptsEmptyValues = false;

    /**
     * The service for verifying the captcha.
     *
     * @var CaptchaFormProtectionInterface
     */
    protected CaptchaFormProtectionInterface $captcha;

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        $this->captcha = GeneralUtility::makeInstance(CaptchaFormProtectionInterface::class);
    }

    /**
     * @inheritdoc
     */
    public function isValid($value): void
    {
        if (!$this->captcha->challenge($value)) {
            $this->addError($this->translateErrorMessage('captcha.error.1679052400', 'FriendlyCaptcha'), 1679052400);
        }
    }
}