const path = require('path');

module.exports = {
    mode: 'production',
    entry: {
        main: {
            import: './Resources/Private/JavaScript/Module.js',
            filename: 'JavaScript/Frontend/Captcha/friendly-captcha.bundle.min.js'
        }
    },
    output: {
        path: path.resolve(__dirname, 'Resources/Public')
    },
    module: {
        rules: [
            {
                test: /\.js$/i,
                exclude: /(node_modules)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                ]
            }
        ]
    }
};